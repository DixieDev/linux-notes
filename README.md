# Arch Linux notes for install and some setup 

## Setting up the 'installer'

- Make bootable Arch USB and boot into it
- Set keyboard layout if not happy with the default (US)
	- Can list layouts with `ls /usr/share/kbd/keymaps/**/*.map.gz`
	- Grep is available so you can do `ls /usr/share/kbd/keymaps/**/*.map.gz | grep 'texttosearchfor'`
	- Can then load the layout with just file name e.g `loadkeys uk` or `loadkeys de-latin1`
- Check the boot mode is UEFI by checking the following command works: `ls /sys/firmware/efi/efivars`
- Connect to the internet
	- Check network devices with `ip link`
	- If device isn't up, use `ip link set devicename up`
		- Whether it's up or not should be in <>s next to the device name in `ip link` output
	- I get an error `RTNETLINK answers: Operation not possible due to RF-kill` when enabling.
		- Because laptop quietly put itself in airplane mode...
	- Run `iwctl` to enter console for connecting to a network
	- Run `device list` and make sure your device is 'on' 
	- Run `station devicename scan`
	- Run `station devicename get-networks` to list available networks
	- Run `station devicename connection deviceSSID` to connect
	- Check all is well by pinging an endpoint, e.g. `ping google.com`
- Update system clock with `timedatectl set-ntp true`
	- Check it worked with `timedatectl status`


## Partitioning

- We will have 3 partitions: An EFI system partition for the bootloader, a root partition for our programs and system files, and a home partition for all of our user files
    - You should always aim to split your root and home partitions if you want an easy life when things go wrong
    - You could in theory more finely tune your partitions, for example adding an additional partition just for games from Steam
- List all disks and partitions with `fdisk -l`. Find the path for your disk of choice
- Run `fdisk /dev/diskfilename` to enter a console for partitioning the disk
- If you have existing partitions that you don't care for, delete them using `d` and then enter `g` to create a new GPT partition table
- Start adding the EFI partition with `n`
    - Press enter to just use the default partition number, taking note of it for a later step
    - Press enter to just pick the first available sector
    - Enter `+300M` to allocate 300 MB for the partition
    - Enter `t` to start changing the EFI partition's type
    - Enter the EFI partition's number
    - Enter `1` to set the partition type to "EFI System"
        - The full list of partition type numbers can be viewed by entering `L` instead
- Start adding the root partition with `n`
    - Press enter to just use the default partition number
    - Press enter to just pick the first available sector
    - Enter how big you want the partition to be, e.g. `+64G` for 64GB
        - 64GB is probably enough for most people, but you can use more if you want
    - Enter `t` to start changing the root partition's type
    - Enter the root partition's number
    - Enter `23` to set the type to Linux Root (x86-64)
        - This is wrong if you're not on x86-64 hardware.
        - View the full list with `L` and find the correct type for your hardware if so
- Start adding the home partition with `n`
    - Press enter to just use the default partition number
    - Press enter to just pick the first available sector
    - Press enter to just fill the remaining contiguous disk space
        - Alternatively you can manually enter how much space you want
    - Enter `t` to start changing the home partition's type
    - Enter the home partition's number
    - Enter `28` to set the type to Linux Home
- Enter `p` and check that you're happy with the partition table
- Enter `w` to write the partition table to the disk and exit fdisk


## Setting up the filesystem 

- Now we need to format each partition with the filesystem we want. We will use ext4
	- Get the paths for your partitions by running `fdisk -l`
	- For the root and home partitions, run `mkfs.ext4 /dev/partitionfilename`
	- For the EFI partition, run `mkfs.fat -F 32 /dev/efipartitionfilename`
		- Be careful that you are only overwriting the one you made and not a pre-existing one
- Next we'll mount our partitions
	- First mount the root partition with `mount /dev/rootpartitionfilename /mnt`
	- Make an EFI folder within the root partition with `mkdir /mnt/efi`
	- Make a home folder within the root partition with `mkdir /mnt/home`
	- Mount the EFI partition with `mount /dev/efipartitionfilename /mnt/efi`
	- Mount the home partition with `mount /dev/homepartitionfilename /mnt/home`


## Installing stuff

- Before we start installing stuff, we should select nearby mirrors for the package manager 
	- Open the file /etc/pacman.d/mirrorlist in Vim
		- Use arrow keys to navigate, `dd` to delete/cut a line, and `p` to paste it
	- Move lines with servers that look to be closer higher up in the list
	- For example, if you are in Europe then move the `.eu` domains upwards
	- Use `:wq` to save and quit. 
- Install the bare necessities by running `pacstrap /mnt base linux linux-firmware`
- We also want a text editor, networking, and documentation access via `man` and `info`:
	- Run `pacstrap /mnt vim` to install Vim 
	- Run `pacstrap /mnt iwd` to install iwd, which we used earlier through `iwctl`
	- Run `pacstrap /mnt dhcpcd` to install a DHCP client
	- Run `pacstrap /mnt man-db man-pages texinfo` to install things for access to docs


## System setup

- Now we'll autogenerate an fstab file to tell the OS how to mount our partitions when booting
	- Run `genfstab -U /mnt >> /mnt/etc/fstab` to generate the file content
	- Run `cat /mnt/etc/fstab` to view the content of the file, making sure paths are correct
- Now change our root into the new system by running `arch-chroot /mnt`
	- This makes the shell behave as if `/mnt` is just `/`, much like when we boot into it later
- Set the timezone by creating a symbolic link from a timezone file to /etc/localtime
	- Run `ln -sf /usr/share/zoneinfo/regionname/cityname /etc/localtime` to create the link
- Use `hwclock --systohc` to generate /etc/adjtime
- Uncomment desired locales in /etc/locale.gen, then run `locale-gen`
- Edit /etc/locale.conf, setting the LANG variable to your locale of choice.
- Edit /etc/vconsole.conf and set KEYMAP to your keyboard layout of choice, from when you used `loadkeys`
- Edit /etc/hostname and type your machine's hostname of choice
- Run `passwd` to set the root user password


## Grub setup

- Install and configure Grub
	- Run `pacman -S grub efibootmgr` to install grub and its related tools
	- If you have another OS (e.g. Windows) installed, set Grub up to include them
		- Run `pacman -S os-prober` to install a tool for Grub to find other OSes to boot
		- Add `GRUB_DISABLE_OS_PROBER=false` to /etc/default/grub
		- Run `fdisk -l` to find the EFI partition for the other OS
		- Run `mount /dev/efipartitionfilename /mnt` to mount the EFI partition
	- Run `mkdir -p /boot/grub`
	- Run `grub-mkconfig -o /boot/grub/grub.cfg` to generate the config
	- If you mounted an EFI partition for a different OS, run `umount /mnt` to unmount it
	- Run `grub-install --target=x86_64-efi --efi-directory=efi --bootloader-id=GRUB`


## Finish up and boot into the machine

- Exit the chroot by running `exit`
- Run `reboot`. 
- On boot, make sure to configure your BIOS to prioritise Grub on boot, and then use Grub to boot into Arch Linux
- Log in using the username `root` and the password you set up earlier.


## Setup basic environment 

- Setup network connection:
	- Run `ip link` and `ip link set devicename up` to enable your wireless device
	- Run `systemctl start systemd-networkd`
	- Run `systemctl enable systemd-networkd`
	- Run `systemctl start iwd`
	- Run `systemctl enable iwd`
	- Run `systemctl start dhcpcd`
	- Run `systemctl enable dhcpcd`
	- Run `iwctl`
	- Connect to your router like you did in the live USB environment
	- Check that your connection works by `ping`ing an endpoint
- Run a full system update by running `pacman -Syyu`
- Run `pacman -S archlinux-keyring`
- Run `pacman -S base-devel git htop` to install a bunch of useful tools
	- base-devel gives you tools that let you compile programs, including those from the AUR
	- Git is useful for version control, and cloning remote repos including those from the AUR
	- htop is a nice command line interface similar to Windows' task manager
- Run `pacman -S sway swaybg waybar` to install Wayland, Sway, and a toolbar for Sway
- Run `pacman -S plasma-desktop` to install KDE Plasma
- Run `pacman -S sddm` to install a simple login screen (display manager).
    - Run `systemctl enable sddm` to enable it.
- Install vi and sudo by running `pacman -S sudo vi`
    - Vi is an older version of Vim with far fewer features and poorer usability overall. It's used by some other programs by default though, such as `sudo`
    - `sudo`, short for "super do" allows authorised non-super users to run stuff as a super user. Any changes to the root partition, for example, are going to require use of `sudo` for example. 
        - Most commonly you will be wanting to use `sudo` whenever installing or uninstalling stuff with Pacman
- Run `visudo`, uncomment the line `%sudo ALL=(ALL) ALL`, then save and quit
    - This authorises anyone in a group called "sudo" to use `sudo`
- Create a group called "sudo" by running `groupadd sudo`
- Now lets create a user so you dont run everything as a root user from here on
	- Run `useradd yourusername` to create your user
	- Run `mkdir /home/yourusername` to create your home directory
	- Run `chown yourusername:yourusername /home/yourusername` to give your user full ownership over their home
	- Run `usermod -aG sudo yourusername` to add yourself to the list of sudoers
	- Run `passwd yourusername` to set your account's password
	- Run `exit` and log back in as your new user
- Run `reboot` and hopefully you should see a login screen with a dropdown at the top allowing you to select a window manager. If you're not comfortable with Sway, start off with KDE Plasma.


## Some useful tools

Some of these can be installed via `pacman`, while for others you will have to learn about the AUR to install.

- Firefox: A web browser
- Alacritty: A fast and nicely configurable terminal emulator
- Thunar: A file browser
- VLC: A media player
- MPV: A light-weight player
- Nomacs: A versatile image viewer
- Neovim: A newish fork of Vim with a number of improvements, including support for Lua-based plugins
- Oguri: Animated GIFs as wallpapers in Wayland
